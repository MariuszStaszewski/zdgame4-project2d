using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Zadanie
//1. Spraw aby ten rodzaj przeciwnika porusza� si� od kraw�dzi ekranu do 
//drugiej kraw�dzi ekranu

namespace FrogNinja.Enemies
{
    public class FullScreenEnemy : BaseEnemy
    {
        Camera mainCamera;

        protected override void Awake()
        {
            base.Awake();

            mainCamera = Camera.main;
        }

        protected override void Move()
        {
            rb.velocity = velocity;

            float screenPositionX = mainCamera.WorldToViewportPoint(transform.position).x;

            if (screenPositionX < 0.03)
            {
                velocity.x = speed;
                enemySprite.flipX = true;

            }
            else if (screenPositionX > 0.97)
            {
                velocity.x = -speed;
                enemySprite.flipX = false;
            }
        }
    }
}

