using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FrogNinja.Enemies
{
    public class HoveringEnemy : BaseEnemy
    {
        [SerializeField] float movementRangeX;

        Vector2 movementRange;

        protected override void Awake()
        {
            base.Awake();

            movementRange = new Vector2(transform.position.x - movementRangeX,
                transform.position.x + movementRangeX);
        }

        protected override void Move()
        {
            rb.velocity = velocity;

            float posX = transform.position.x;

            if (posX < movementRange.x)
            {
                velocity.x = speed;
                enemySprite.flipX = true;

            }
            else if (posX > movementRange.y)
            {
                velocity.x = -speed;
                enemySprite.flipX = false;
            }
        }
    }
}

