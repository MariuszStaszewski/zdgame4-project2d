using FrogNinja.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FrogNinja.Enemies
{
    public abstract class BaseEnemy : MonoBehaviour
    {
        [SerializeField] protected Rigidbody2D rb;
        [SerializeField] protected float speed;

        protected Vector2 velocity;

        [SerializeField] protected SpriteRenderer enemySprite;

        protected virtual void Awake()
        {
            velocity = new Vector2(speed, 0);
            enemySprite.flipX = true;
        }

        protected virtual void FixedUpdate()
        {
            Move();
        }

        protected abstract void Move();

        protected virtual void OnCollisionEnter2D(Collision2D collision)
        {
            if(collision.rigidbody.GetComponent<PlayerController>() != null)
            {
                EventManager.OnEnemyHitPlayer();
            }
            else
            {
                rb.gameObject.SetActive(false);
                EventManager.OnEnemyDied();
            }
        }
    }
}

