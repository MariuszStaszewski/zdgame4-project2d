using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Zadanie
//1. Spraw aby po wci�ni�ciu lewego przycisku myszy 
//zespawnowa� si� pocisk i lecia� w g�r�
//2. Pocisk niech ginie po 3 sekundach

//Zadanie 2
//1. Po zderzeniu si� pocisku z przeciwnikiem
//zniszcz pocisk
//wy��cz rigdbody i kolizje na przeciwniku

//Zadanie 3
//1. Spraw aby gracz nie m�g� strzela� kiedy nie jest w gameplayu
//2. Zr�b to r�wnie� dla animation controllera

namespace FrogNinja.Player
{
    [RequireComponent(typeof(PlayerController))]
    public class PlayerShootingController : MonoBehaviour
    {
        PlayerController playerController;
        [SerializeField] PlayerBullet bulletPrefab;
        [SerializeField] AudioClip shootSFX;

        Camera mainCamera;
        private void Awake()
        {
            playerController = GetComponent<PlayerController>();
            mainCamera = Camera.main;
        }

        private void Update()
        {
            if (!playerController.IsActive)
                return;

            if(Input.GetMouseButtonUp(0))
            {
                SpawnBullet();
            }
        }

        void SpawnBullet()
        {
            PlayerBullet spawnedBullet = GameObject.Instantiate<PlayerBullet>(bulletPrefab,
                transform.position, Quaternion.identity);

            Vector3 direction = Vector3.up;

            Vector3 worldMousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
            worldMousePosition.z = transform.position.z;

            direction = worldMousePosition - transform.position;

            spawnedBullet.Shoot(direction.normalized);

            AudioSystem.PlaySFX_Global(shootSFX);
        }
    }
}

