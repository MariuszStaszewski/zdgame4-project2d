using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FrogNinja.Player
{
    public class PlayerBullet : MonoBehaviour
    {
        [SerializeField] float speed;
        [SerializeField] Rigidbody2D rb;
        [SerializeField] AudioClip impactSound;

        Vector3 direction = Vector3.zero;

        public void Shoot(Vector3 newDirection)
        {
            direction = newDirection;
            Invoke("Die", 3f);
        }

        private void FixedUpdate()
        {
            rb.velocity = direction * speed;
        }

        void Die()
        {
            Destroy(gameObject);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            AudioSystem.PlaySFX_Global(impactSound);

            Die();
        }
    }
}

