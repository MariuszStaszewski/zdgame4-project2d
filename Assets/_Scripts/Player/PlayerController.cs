using FrogNinja.LevelGeneration;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace FrogNinja.Player
{

    public class PlayerController : MonoBehaviour
    {
        [SerializeField] float horizontalSpeed = 10f;
        [SerializeField] Rigidbody2D rb;
        [SerializeField] AudioClip lossSFX;
        public bool IsActive { get { return active; } }

        float horizontalInput;
        Vector3 tempVelocity, screenPosition;
        Camera mainCamera;
        bool wrapScreen;

        bool active = false;

        private void Awake()
        {
            LevelGenerator.LevelGenerated += LevelGenerator_LevelGenerated;
        }

        private void OnDestroy()
        {
            LevelGenerator.LevelGenerated -= LevelGenerator_LevelGenerated;
        }

        private void LevelGenerator_LevelGenerated(Vector3 obj)
        {
            transform.position = obj;
        }

        void Start()
        {
            mainCamera = Camera.main;
        }

        void Update()
        {
            if (!active)
                return;

            horizontalInput = Input.GetAxis("Horizontal");
            HandleScreenWrap();
            EventManager.OnUpdatePlayerPosition(transform.position);
        }

        private void HandleScreenWrap()
        {
            screenPosition = mainCamera.WorldToViewportPoint(transform.position);

            if (screenPosition.x < -0.03f)
            {
                screenPosition.x = 1.03f;
                wrapScreen = true;
            }
            else if (screenPosition.x > 1.03f)
            {
                screenPosition.x = -0.03f;
                wrapScreen = true;
            }
            else
            {
                wrapScreen = false;
            }

            CheckLossCondition();
        }

        private void CheckLossCondition()
        {
            if (screenPosition.y < -0.2f)
            {
                EventManager.OnPlayerDied();
            }
        }

        private void FixedUpdate()
        {
            if (!active)
                return;

            tempVelocity = rb.velocity;
            tempVelocity.x = horizontalInput * horizontalSpeed;
            rb.velocity = tempVelocity;

            if (wrapScreen)
                rb.MovePosition(Camera.main.ViewportToWorldPoint(screenPosition));
        }

        public void SwitchState(bool state, bool playSFX = true)
        {
            active = state;

            if (rb != null)
                rb.simulated = state;

            if (!state && playSFX)
                AudioSystem.PlaySFX_Global(lossSFX);
        }
    }
}