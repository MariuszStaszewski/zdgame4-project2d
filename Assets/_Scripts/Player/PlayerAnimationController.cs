using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FrogNinja.Player
{
    [RequireComponent(typeof(PlayerController))]
    public class PlayerAnimationController : MonoBehaviour
    {
        PlayerController playerController;
        [SerializeField] SpriteRenderer playerSprite;
        [SerializeField] Animator playerAnimator;
        [SerializeField] Rigidbody2D rb;

        bool flipSprite = false;
        // Update is called once per frame

        private void Awake()
        {
            playerController = GetComponent<PlayerController>();
        }

        private void Start()
        {
            playerAnimator.SetBool("InAir", true);
        }

        void Update()
        {
            if (!playerController.IsActive)
                return;

            if (Input.GetAxisRaw("Horizontal") == -1)
                flipSprite = true;
            else if (Input.GetAxisRaw("Horizontal") == 1)
                flipSprite = false;

            playerSprite.flipX = flipSprite;
        }

        private void FixedUpdate()
        {

        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            playerAnimator.SetBool("InAir", false);
        }

        private void OnCollisionExit2D(Collision2D collision)
        {
            playerAnimator.SetBool("InAir", true);
        }
    }
}