using FrogNinja.Player;
using FrogNinja.UI;
using UnityEngine;

namespace FrogNinja.States
{
    //Zadanie 
    //1. W��cz pauze kiedy wci�niemy przycisk pauzy na HUDzie, albo kiedy 
    //wci�niemy 'P' na klawiaturze.
    //2. Na HUDzie w trakcie pauzy poka� du�y napis "PAUSE"
    //3. Wy��cz Pauze po wci�ni�ciu ponownie przycisku albo 'P'
    //4. Gracz nie powinien m�c strzela� w trakcie pauzy

    public class GameState : BaseState
    {
        PlayerController playerController;
        bool gamePaused = false;

        public GameState(StateMachine stateMachine)
        {
            Initialize(stateMachine);
        }

        public override void EnterState()
        {
            Debug.Log("Enter GameState");
            AudioSystem.SwitchMusic_Global(true);
            playerController = GameObject.FindObjectOfType<PlayerController>();
            playerController.SwitchState(true);

            EventManager.PlayerDied += EventManager_PlayerDied;
            EventManager.EnemyHitPlayer += EventManager_EnemyHitPlayer;
            EventManager.EnterPause += EventManager_EnterPause;

            UIManager.Instance.ShowHUD();
        }

        private void EventManager_EnterPause()
        {
            PauseGame();
        }

        void PauseGame()
        {
            gamePaused = !gamePaused;
            EventManager.OnPause(gamePaused);

            if (gamePaused)
                Time.timeScale = 0;
            else
                Time.timeScale = 1;

            playerController.SwitchState(!gamePaused, false);

            AudioSystem.PlayPauseSFX_Global(gamePaused);
            AudioSystem.SwitchMusic_Global(!gamePaused);
        }

        private void EventManager_EnemyHitPlayer()
        {
            EventManager.OnPlayerDied();
        }

        public override void ExitState()
        {
            if (playerController != null)
                playerController.SwitchState(false);

            EventManager.PlayerDied -= EventManager_PlayerDied;
            EventManager.EnemyHitPlayer -= EventManager_EnemyHitPlayer;
            EventManager.EnterPause -= EventManager_EnterPause;

            AudioSystem.SwitchMusic_Global(false);

            Debug.Log("Exit GameState");
        }

        public override void UpdateState()
        {
            if(Input.GetKeyUp(KeyCode.P))
            {
                PauseGame();
            }
        }

        private void EventManager_PlayerDied()
        {
            GoToLose();
        }

        private void GoToMenu()
        {
            myStateMachine.EnterState(new MenuState(myStateMachine));
        }

        private void GoToLose()
        {
            myStateMachine.EnterState(new LoseState(myStateMachine));
        }
    }
}

