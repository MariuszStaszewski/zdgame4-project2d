using FrogNinja.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FrogNinja.States
{
    public class MenuState : BaseState
    {
        public MenuState(StateMachine stateMachine)
        {
            Initialize(stateMachine);
        }

        public override void EnterState()
        {
            AudioSystem.SwitchMusic_Global(true);

            EventManager.EnterGameplay += EventManager_EnterGameplay;
            UIManager.Instance.ShowMainMenu();

            Debug.Log("Enter Menu State");
        }

        private void EventManager_EnterGameplay()
        {
            GoToGame();
        }

        public override void ExitState()
        {
            EventManager.EnterGameplay -= EventManager_EnterGameplay;
            Debug.Log("Exit Menu State");
        }

        public override void UpdateState()
        {

        }

        private void GoToGame()
        {
            myStateMachine.EnterState(new GameState(myStateMachine));
        }
    }
}

