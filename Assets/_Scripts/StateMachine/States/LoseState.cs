using FrogNinja.UI;
using UnityEngine;

namespace FrogNinja.States
{
    //Zadanie:
    //0. Przygotuj ekran LoseWindow w Unity- z przynajmniej dwoma przyciskami (Restart, Main Menu)
    //1. Jak gracz spadnie z ekranu, zamiast restartowa� gr�, przejd� do LoseState
    //2. Na wej�cie do stanu LoseState w��cz okno LoseWindow
    //3. Obydwa przyciski w LoseWindow w chwili obecnej powinien restartowa� scen� tak jak si� dzieje obecnie.

    //Pami�taj o wykorzystaniu event�w z EventManagera

    public class LoseState : BaseState
    {
        public LoseState(StateMachine stateMachine)
        {
            Initialize(stateMachine);
        }

        public override void EnterState()
        {
            Debug.Log("Enter LoseState");
            UIManager.Instance.ShowLoseWindow();

            EventManager.EnterGameplay += EventManager_EnterGameplay;
            EventManager.EnterMenu += EventManager_EnterMenu;
        }

        private void EventManager_EnterGameplay()
        {
            GoToGame();
        }

        private void EventManager_EnterMenu()
        {
            GoToMenu();
        }

        public override void ExitState()
        {
            Debug.Log("Exit LoseState");
            EventManager.EnterGameplay -= EventManager_EnterGameplay;
            EventManager.EnterMenu -= EventManager_EnterMenu;
        }

        public override void UpdateState()
        {
        }

        private void GoToMenu()
        {
            myStateMachine.EnterState(new MenuState(myStateMachine));
        }

        private void GoToGame()
        {
            myStateMachine.EnterState(new GameState(myStateMachine));
        }

    }
}

