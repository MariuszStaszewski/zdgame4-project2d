using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Zadanie:
//Ud�wi�kuj gr�
//-dodaj d�wi�k na odbicie si� od platformy
//-dodaj d�wi�k na strza�
//-dodaj d�wi�k na przegran� gracza
//-dodaj d�wi�k na pobicie high score (na ekranie pora�ki)


//Zadanie 2:
//Spraw aby muzyka gra�a jedynie w menu g��wnym i w trakcie gameplayu
public class AudioSystem : MonoBehaviour
{
    #region Global
    private static AudioSystem Instance;

    public static void PlaySFX_Global(AudioClip audioClip)
    {
        if (Instance == null)
            return;

        //PLAY SFX
        Instance.PlaySFX_Local(audioClip);
    }

    public static void PlayButtonSFX_Global()
    {
        if (Instance == null)
            return;

        Instance.PlaySFX_Local(Instance.buttonSFX);
    }

    public static void SwitchMusic_Global(bool play)
    {
        if (Instance == null)
            return;

        if (!play)
            Instance.musicSource.Pause();
        else if (!Instance.musicSource.isPlaying)
        {
            Instance.musicSource.Play();
        }
    }

    public static void PlayPauseSFX_Global(bool pause)
    {
        if (Instance == null)
            return;

        if (pause)
            Instance.sfxSource.PlayOneShot(Instance.pauseSFX);
        else
            Instance.sfxSource.PlayOneShot(Instance.unpauseSFX);
    }
    #endregion

    #region Local

    [SerializeField] AudioSource sfxSource;
    [SerializeField] AudioSource musicSource;

    [SerializeField] AudioClip buttonSFX;

    [SerializeField] AudioClip pauseSFX, unpauseSFX;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }

    void PlaySFX_Local(AudioClip audioClip)
    {
        sfxSource.PlayOneShot(audioClip);
    }
    #endregion
}
