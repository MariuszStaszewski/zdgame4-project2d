using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ButtonClick : MonoBehaviour
{
    Button myButton;

    private void Awake()
    {
        myButton = GetComponent<Button>();

        myButton.onClick.AddListener(PlayClick);
    }

    void PlayClick()
    {
        AudioSystem.PlayButtonSFX_Global();
    }
}
