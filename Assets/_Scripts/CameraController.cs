using FrogNinja.LevelGeneration;
using FrogNinja.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FrogNinja.CameraControls
{
    [RequireComponent(typeof(Camera))]
    public class CameraController : MonoBehaviour
    {
        //Zadanie:
        //1. Spraw aby pozycja Y jedynie ros�a
        //2. Ustaw kamer� w taki spos�b aby by�o widoczne wi�cej na ekranie (posta� na �rodku)

        [SerializeField] PlayerController playerCharacter;
        Vector3 tempPosition;

        private void Awake()
        {
            LevelGenerator.LevelGenerated += LevelGenerator_LevelGenerated;
        }

        private void OnDestroy()
        {
            LevelGenerator.LevelGenerated -= LevelGenerator_LevelGenerated;
        }

        private void LevelGenerator_LevelGenerated(Vector3 obj)
        {
            tempPosition.y = obj.y + 4.260963f;
            transform.position = tempPosition;
        }

        private void Start()
        {
            tempPosition = transform.position;
        }

        private void Update()
        {
            if (playerCharacter.transform.position.y > tempPosition.y)
            {
                tempPosition.y = playerCharacter.transform.position.y;
                transform.position = tempPosition;
            }
        }
    }
}

