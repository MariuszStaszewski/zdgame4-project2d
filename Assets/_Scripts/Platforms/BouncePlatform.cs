using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FrogNinja.Platforms
{

    //Zadanie:
    //1. Spraw aby gracz odbi� si� od platformy w trakcie kolizji
    //2. Uzale�nij si�� odbicia od parametru sterowanego z poziomu platformy
    //3. W��cz jednostronn� kolizj� (kolizja powinna nast�powa� jedynie kiedy posta� gracz
    // l�duje na platformie.

    public class BouncePlatform : BasePlatform
    {
        [SerializeField] float bounceStrength = 8f;
        [SerializeField] AudioClip bounceSFX;

        protected override void HandleCollision(Collision2D collision)
        {
            AudioSystem.PlaySFX_Global(bounceSFX);

            collision.rigidbody.velocity = Vector3.zero;
            collision.rigidbody.AddForce(Vector2.up * bounceStrength, ForceMode2D.Impulse);
        }
    }
}

