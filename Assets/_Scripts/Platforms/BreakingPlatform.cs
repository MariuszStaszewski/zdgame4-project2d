using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FrogNinja.Platforms
{
    public class BreakingPlatform : BasePlatform
    {
        //Zadanie - warunek: bez Destroy() i DestroyImmediate()
        //1. Spraw aby ten rodzaj platformy niszczy� si� natychmiast przy l�dowaniu na ni� przez
        //posta� gracza
        //2. Wy��cz sprite 
        [SerializeField] SpriteRenderer mySprite;
        protected override void HandleCollision(Collision2D collision)
        {
            collision.otherCollider.enabled = false;
            mySprite.enabled = false;
        }
    }
}

