using FrogNinja.Enemies;
using FrogNinja.LevelGeneration.Configs;
using FrogNinja.Platforms;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FrogNinja.LevelGeneration
{
    public class LevelGenerator : MonoBehaviour
    {
        public static event System.Action<Vector3> LevelGenerated;

        //Zadanie
        //1. Dodaj losow� wariacj� w spawnowaniu platform w pozycji Y
        //2. Dodaj zasad� generewania poziom�w: ta sama platforma nie mo�e wyst�pi� wi�cej ni� dwa razy pod rz�d
        //3. Wyj�tek: istnieje tylko jedna konfiguracje
        //4. Wyj�tek do wyj�tku: chyba, �e konfiguracja zawiera wi�cej ni� 1 prefab

        [SerializeField] List<PlatformConfiguration> platformConfigurations;
        [SerializeField] List<BaseEnemy> enemies;

        [SerializeField] Transform playerSpawnPosition;
        [SerializeField] Vector2 minMaxVariation = new Vector2(0f, 1f);
        [SerializeField] float minDistanceToSpawnEnemy = 0.5f;

        [SerializeField] int maxCountOfTheSamePlatformInRow = 1;

        List<BasePlatform> spawnedPlatforms = new List<BasePlatform>();
        List<BaseEnemy> spawnedEnemies = new List<BaseEnemy>();

        BasePlatform lastSpawnedPlatform;
        int samePlatformInRow = 0;

        float lastSpawnedY;
        Camera mainCamera;
        bool enableEnemySpawn = false;

        private void Awake()
        {
            mainCamera = Camera.main;

            EventManager.EnterGameplay += EventManager_EnterGameplay;
        }

        private void OnDestroy()
        {
            EventManager.EnterGameplay -= EventManager_EnterGameplay;
            EventManager.PlayerPositionUpdate -= EventManager_PlayerPositionUpdate;
            EventManager.PlayerDied -= EventManager_PlayerFallenOff;
        }

        private void EventManager_EnterGameplay()
        {
            enableEnemySpawn = false;
            CreateStartLevel();
            enableEnemySpawn = true;

            if(LevelGenerated != null)
            {
                LevelGenerated(playerSpawnPosition.position);
            }

            EventManager.PlayerPositionUpdate += EventManager_PlayerPositionUpdate;
            EventManager.PlayerDied += EventManager_PlayerFallenOff;
        }

        private void EventManager_PlayerFallenOff()
        {
            EventManager.PlayerPositionUpdate -= EventManager_PlayerPositionUpdate;
            EventManager.PlayerDied -= EventManager_PlayerFallenOff;
        }

        private void EventManager_PlayerPositionUpdate(Vector3 obj)
        {
            if(obj.y > lastSpawnedY / 2)
            {
                SpawnPlatform();
            }
        }

        void CreateStartLevel()
        {
            DestroyPreviousLevel();

            lastSpawnedY = playerSpawnPosition.position.y;

            for (int i = 0; i < 20; i++)
            {
                SpawnPlatform();
            }
        }

        private void DestroyPreviousLevel()
        {
            if (spawnedPlatforms.Count > 0)
            {
                for (int i = 0; i < spawnedPlatforms.Count; i++)
                {
                    Destroy(spawnedPlatforms[i].gameObject);
                }

                spawnedPlatforms.Clear();
            }

            if (spawnedEnemies.Count > 0)
            {
                for (int i = 0; i < spawnedEnemies.Count; i++)
                {
                    Destroy(spawnedEnemies[i].gameObject);
                }

                spawnedEnemies.Clear();
            }
        }

        private void SpawnPlatform()
        {
            BasePlatform platformToSpawn = DifferentiatePlatforms(out PlatformConfiguration configToUse);
            Vector3 spawnPosition = new Vector3(GetRandomXPosition(), GetRandomYPosition(configToUse), 0);

            BasePlatform spawnedPlatform = GameObject.Instantiate<BasePlatform>(platformToSpawn, spawnPosition, Quaternion.identity, transform);

            spawnedPlatforms.Add(spawnedPlatform);

            SpawnEnemyIfAvailable(lastSpawnedY, spawnPosition.y);

            lastSpawnedY = spawnPosition.y;
        }

        void SpawnEnemyIfAvailable(float lastY, float currentY)
        {
            if (!enableEnemySpawn)
                return;

            float difference = currentY - lastY;

            if(difference > minDistanceToSpawnEnemy)
            {
                Vector3 spawnPosition = new Vector3(GetRandomXPosition(), lastY + (difference / 2), 0);

                BaseEnemy enemyToSpawn = enemies[Random.Range(0, enemies.Count)];

                BaseEnemy spawnedEnemy = GameObject.Instantiate<BaseEnemy>(enemyToSpawn, spawnPosition, Quaternion.identity, transform);

                spawnedEnemies.Add(spawnedEnemy);
            }
        }

        private BasePlatform DifferentiatePlatforms(out PlatformConfiguration configToUse)
        {
            configToUse = platformConfigurations[Random.Range(0, platformConfigurations.Count)];

            BasePlatform platformToSpawn = configToUse.GetRandomPlatform();

            if(platformConfigurations.Count == 1)
            {
                if (platformConfigurations[0].Platforms.Count == 1)
                    return platformToSpawn;
            }

            if (platformToSpawn == lastSpawnedPlatform)
            {
                samePlatformInRow++;

                if (samePlatformInRow >= maxCountOfTheSamePlatformInRow)
                {
                    while (platformToSpawn == lastSpawnedPlatform)
                    {
                        configToUse = platformConfigurations[Random.Range(0, platformConfigurations.Count)];
                        platformToSpawn = configToUse.GetRandomPlatform();
                    }

                    samePlatformInRow = 0;
                }
            }
            else
            {
                samePlatformInRow = 0;
            }

            lastSpawnedPlatform = platformToSpawn;
            return platformToSpawn;
        }

        private float GetRandomYPosition(PlatformConfiguration configToUse)
        {
            return lastSpawnedY + configToUse.DefaultYIncrease + Random.Range(minMaxVariation.x, minMaxVariation.y);
        }

        float GetRandomXPosition()
        {
            float randomValue = Random.Range(0f, 1f);

            Vector3 resultPosition = mainCamera.ViewportToWorldPoint(new Vector3(randomValue, 0));

            return resultPosition.x;
        }
    }
}

