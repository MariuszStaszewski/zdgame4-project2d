using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    const string HIGH_SCORE_KEY = "high_score";
    [SerializeField] int positionDifferenceScoreMultiplier = 100;

    [SerializeField] AudioClip highScoreSFX;
    int currentScore, highScore;

    public int CurrentScore { get { return currentScore; } }
    public int HighScore { get { return highScore; } }

    float maxPositionY;
    bool firstUpdate = true;
    private void Awake()
    {
        highScore = PlayerPrefs.GetInt(HIGH_SCORE_KEY, 0);

        EventManager.EnterGameplay += EventManager_EnterGameplay;
        EventManager.PlayerPositionUpdate += EventManager_PlayerPositionUpdate;
        EventManager.PlayerDied += EventManager_PlayerFallenOff;
    }

    private void OnDestroy()
    {
        EventManager.EnterGameplay -= EventManager_EnterGameplay;
        EventManager.PlayerPositionUpdate -= EventManager_PlayerPositionUpdate;
        EventManager.PlayerDied -= EventManager_PlayerFallenOff;
    }

    private void EventManager_EnterGameplay()
    {
        firstUpdate = true;
        currentScore = 0;
    }

    private void EventManager_PlayerPositionUpdate(Vector3 obj)
    {
        if(firstUpdate)
        {
            maxPositionY = obj.y;
            currentScore = 0;
            firstUpdate = false;
            return;
        }

        if(obj.y > maxPositionY)
        {
            //ADD POINTS
            float difference = obj.y - maxPositionY;
            currentScore += (int)(difference * positionDifferenceScoreMultiplier);

            maxPositionY = obj.y;

            EventManager.OnUpdateScore(currentScore);
        }    
    }

    private void EventManager_PlayerFallenOff()
    {
        SaveHighScore();
    }

    void SaveHighScore()
    {
        if(currentScore > highScore)
        {
            PlayerPrefs.SetInt(HIGH_SCORE_KEY, currentScore);
            highScore = currentScore;

            Invoke("PlayJingle", 1.5f);
        }
    }

    void PlayJingle()
    {
        AudioSystem.PlaySFX_Global(highScoreSFX);
    }
}
