using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Zadanie
// 1. Zbieraj wszystkie zespawnowane platformy w List<BasePlatform> w generatorze
// 2. Po rozpocz�ciu gry ustaw gracza w pozycji playerSpawnPosition z klasy LevelGenerator
// 2'. Zresetuj pozycje kamery
// 3. Po rozpocz�ciu gry (je�eli istniej� platformy), zniszcz wszystkie ju� istniej�ce i wygeneruj nowy poziom
// 4. Z UI w LoseWindow wywo�aj prawid�owe metody w LoseState �eby zmieni� stan aplikacji

namespace FrogNinja.UI
{
    public class LoseWindow : BaseWindow
    {
        [SerializeField] GameObject textObjectHolder;
        [SerializeField] private TMPro.TMP_Text currentScoreValue, highScoreValue;
        public override void ShowWindow()
        {
            base.ShowWindow();

            //Zadanie:
            //0. Przygotuj texty dla obecnego i najwy�szego wyniku na UI
            //1. Znajd� instancje ScoreManagera u�ywaj�c FindObjectOfType
            //FindObjectOfType<>
            //2. Je�eli instancji nie ma, ukryj napis CurrentScore i HighScore
            //3. Dodaj spos�b na otrzymanie warto�ci z Score Managera
            //4. Uzupe�nij UI pozyskanymi warto�ciami
            ScoreManager scoreManager = FindObjectOfType<ScoreManager>();

            bool hasScoreManger = scoreManager != null;
            textObjectHolder.SetActive(hasScoreManger);

            if (!hasScoreManger)
                return;

            currentScoreValue.text = scoreManager.CurrentScore.ToString();
            highScoreValue.text = scoreManager.HighScore.ToString();
        }

        public void Button_Restart()
        {
            EventManager.EnterGameplayButton();
        }

        public void Button_MainMenu()
        {
            EventManager.EnterMenuButton();
        }
    }
}

