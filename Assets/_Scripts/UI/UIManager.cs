using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FrogNinja.UI
{
    public class UIManager : MonoBehaviour
    {
        public static UIManager Instance;

        [SerializeField] MainMenuWindow mainMenu;
        [SerializeField] HUDWindow hud;
        [SerializeField] LoseWindow loseWindow;
        BaseWindow currentlyOpenWindow;

        private void Awake()
        {
            if (Instance != null)
            {
                Destroy(this.gameObject);
                return;
            }

            Debug.Log($"{name} is initialized");
            Instance = this;
        }

        public void ShowMainMenu()
        {
            HideAndSwitchWindow(mainMenu);
        }

        public void ShowHUD()
        {
            HideAndSwitchWindow(hud);
        }

        public void ShowLoseWindow()
        {
            HideAndSwitchWindow(loseWindow);
        }

        private void HideAndSwitchWindow(BaseWindow windowToSwitchTo)
        {
            if (currentlyOpenWindow != null)
                currentlyOpenWindow.HideWindow();

            currentlyOpenWindow = windowToSwitchTo;

            currentlyOpenWindow.ShowWindow();
        }

        public void ShowFail()
        {
            Debug.Log($"Show fail from {name}");
        }
    }
}

